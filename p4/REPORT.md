# Práctica 4: VideoTask

Autor Sergio Varona Madrid

Fecha 17/12/1999

## Prefacio

El objetivo de esta práctica era realizar la paralelización de un problema de filtrado de imágenes bidimensionales mediante paralelismo asíncrono y flexible en el número de threads.Esto lo he implementado usando las nuevas directivas task,taskwait y master que explicaré más adelante.
En cuanto a los objetivos conseguidos con esta práctica podemos destacar que hemos profundizado aún más en las diferencias de ejecución secuencial y paralalela(openMP).
Además, he aprendido que para programas que tardan bastante en ejecutarse, como es este caso, puedo reducir el valor de aquellas variables que afecten al tiempo de ejecución, que en esta práctica son la resolución(width x height) y sum, que indica el número de imágenes del vídeo.



## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema
-Ejecución sobre partición hdd:

  - Linux Mint 19.2
  - Architecture:        x86_64
  - CPU op-mode(s):      32-bit, 64-bit
  - Byte Order:          Little Endian
  - CPU(s):              8
  - On-line CPU(s) list: 0-7
  - Thread(s) per core:  2
  - Core(s) per socket:  4
  - Socket(s):           1
  - NUMA node(s):        1
  - Vendor ID:           GenuineIntel
  - CPU family:          6
  - Model:               158
  - Model name:          Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz
  - Stepping:            9
  - CPU MHz:             2800.103
  - CPU max MHz:         2800,0000
  - CPU min MHz:         800,0000
  - BogoMIPS:            5616.00
  - Virtualization:      VT-x
  - L1d cache:           32K
  - L1i cache:           32K
  - L2 cache:            256K
  - L3 cache:            6144K
  - NUMA node0 CPU(s):   0-7
  - Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp


-Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i7-7700HQ
  - CPU 8 Cores: 1 socket, 2 threads/core, 4 cores/socket
  - Kernel: Linux 4.15.0-54-generic
  - CPU flags: sse, sse2, ssse3, fma, fma4 y avx
  - CPU @ 2.80GHz
  - CPU: L2: 256KiB, L3: 6144KiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre HDD

## 2. Diseño e Implementación del Software
En cuanto al diseño e implementación, se nos pide paralelizar de forma asíncrona aquella parte del código del archivo video_task que veamos más adecuada con el objetivo de mejorar los tiempos de ejecución, en mi caso, he escogido paralelizar el bucle do/while con el objetivo de repartir las llamadas a la función fgauss entre los threads. El encargado de crear las tareas, será el hilo maestro
```
#pragma omp parallel
{

  #pragma omp master
  {

    do
    {

      size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

    ...

```
Los hilos hijo irán cogiendo las tareas creadas por el master de la cola de forma asíncrona

```
if (size)
      {
        #pragma omp task
        {
          fgauss (pixels[i], filtered[i], height, width);
        }
        ...
```
Se hacen las llamadas a fgauss y cuando todos los hilos terminen, se produce la sincronización y se escribe en el fichero de salida

```
if(i==seq-1)
{
  #pragma omp taskwait
  i=0;
  for (int j = 0; j<seq;j++)
  {
    fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
  }
}else{
  i++;
}
}
      ...
```
Esta última parte del código la explico mejor en las respuestas a las preguntas planteadas más adelante.

## 3. Metodología y desarrollo de las pruebas realizadas
En cuanto al benchmarking, antes de comenzar, volvemos a tener en cuenta aquellos aspectos que hemos tenido en cuenta en prácticas anteriores y que no explicaste en clase, esto es, tener el portátil conectado a la corriente, sin ningún proceso innecesario abierto, fijando el governor a userspace y fijando la misma frecuencia de los 8 cores de mi sistema.En este caso, he escogido una frecuencia mayor que en prácticas anteriores(2.8GHz), ya que en esta práctica no se pide llegar a un tiempo de ejecución determinado con la versión secuencial.<br>

Entrando más en detalle en los benchmarks, mi objetivo es mostrar la mejora en el speedup de la versión paralela frente a la secuencial para distintos valores de resolución y sum.<br>
Las resoluciones que voy a utilizar son las siguientes:<br>


| Resolución | Width   | Height |
| ------ |---------| ------:|
| 144p  | 256   | 144    |
| 240p  |   320  | 240   |
| 360p | 480 |  360  |
| 480p  | 720   |  480   |
| 720p  |  1280   | 720   |
| 1080p| 1920 | 1080   |

Como ejemplo antes de las gráficas voy a mostrar las ganancias(speedup) para max=80 que es el valor por defecto.

| Resolución | seq   | openMP |
| ------ |---------| ------:|
| 144p  | 1   |  3,237   |
| 240p  |   1  |   3,258 |
| 360p | 1 |  3,291|
| 480p  | 1   | 3,287|
| 720p  |  1   | 3,347|
| 1080p| 1 |   3,098 |

Podemos  ver de primeras que el speedup se para openMP se mantiene más o menos constante independientemente de la resolución para max=80.<br>

He decidido realizar 4 gráficas, en las 3 primeras muestro la comparación del speedup entre secuencial/openMP para distintas resoluciones y distintos valores de max(10,80,120).<br>
Por último, recojo los valores del speedup de openMP de las 3 gráficas anteriores y los muestro en la última gráfica para ver si varía el speedup para distintas resoluciones con distintos valores de max.


Entrando más en detalle en la primera gráfica, podemos observar que a medida que aumenta la resolución el speedup mejora con bastante.Va de 1,38 para 144p a 2,95 para 1080p
![](images/benchmark1.png)

En cuanto a las siguientes 2 gráficas observamos que el speedup se mantiene más o menos constante para valores grandes de max(80 y 120). Observamos que el speedup se mantiene por encima de 3, que es casi el doble que para max=10 y 144p.
![](images/benchmark2.png)

![](images/benchmark3.png)

Por lo que llegamos a la conclusión apoyandonos en esta última gráfica de que para valores pequeños de max, se produce un aumento del speedup en función de los valores de la resolución y que llegado un momento(cierto valor de max), el speedup se mantiene constante con pequeñas oscilaciones.
![](images/benchmark4.png)

## 4. Discusión
Antes de nada comentar que para realizar los análisis he usado la API gettimeofday+ROI para calcular los tiempos de ejecución tanto de la versión secuencial como de la implementada con openMP.<br>

En cuanto a las dificultades encontradas realizando la práctica, podemos destacar que, en un primer momento, el código me ha parecido difícil de entender ya que había funciones que eran nuevas para mí como memset,memcpy o fflush, otros elementos como dobles punteros y otras funciones que aun no manejaba con soltura como son aquellos que tienen que ver con la entrada/salida (fwrite,fread).<br>

**1. Explica qué funcionalidad has tenido que añadir/modificar sobre el código original. Explica si has encontrado algún fallo en la funcionalidad (“lo que hace el programa”, independientemente al paralelismo) y cómo lo has corregido. Atención: encontrar el fallo es algo “para nota”, por lo que no te deberías preocupar si no lo encuentras.**<br>
La funcionalidad que hay que añadir respecto del código original(a parte de las directivas openMP) es el buffer que se encargará de ir almacenando las tareas, una vez hayan terminado de llamar a la función fgauss, escribirán en el fichero de salida mediante la función fwrite.

```
if(i==seq-1)
        {
          #pragma omp taskwait
          i=0;
          for (int j = 0; j<seq;j++)
          {
            fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
          }
        }else{
          i++;
        }
      }
```
En cambio, en la versión secuencia lo que se hacia era una llamada a fwrite por cada llamada a la función fgauss.<br>
Además, aunque no se pide explicitamente, tenemos que introducir un nuevo bucle al final para controlar el resto en caso de que max%seq!=0 ya que el resultado del secuencial y el paralelo serían distintos.

```

Termina sección paralalela
...
for(int k=0;k<i;k++){
  fwrite(filtered[k], (height + 2) * (width + 2) * sizeof(int), 1, out);
}

```

**2. Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado, salvo las que ya hayas explicado en la práctica anterior (por ejemplo parallel).**<br>
Las primera directiva que he utilizado(a parte de la de parallel) es la directiva *master* con el objetivo de especificar, que esa parte del código solo será ejecutada por el thread master, por lo que el resto de threads se saltan esta parte del código.El objetivo de esto, es que el master genere las tareas y el resto de hilos las va cogiendo.<br>
La siguiente directiva utilizada es *task*, la utilizo para indicar que las tareas a realizar son unidades de trabajo independiente y que los hilos van cogiendo las tareas del master de forma asíncrona. Posteriormente, añado la directiva *taskwait* para esperar hasta que todas las tareas generadas hayan finalizado y posteriormente escribir en el fichero de salida.

**3. Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.**<br>
Las etiquetas utilizadas en la región paralela han sido únicamente firstprivate y shared. La primera se ha utilizado para la variable size, con el objetivo de declararla privada e inicilizarla con los valores anteriores(los del master).Esto se debe a que cada thread hijo va a tener una copia local de la variable size que es obtenida a través del hilo master.
El resto de variables(i,height,width,pixels,filtered,in,out,seq) las he declarado como shared ya que son únicamente de lectura y no vamos a alterar sus valores.


**4. Explica cómo se consigue el paralelismo en este programa (aplicable sobre la función main).**
El paralelismo se consigue dividiendo las llamadas de la función fgauss dentro del bucle do/while entre los hilos hijo. Gracias a esto, el hilo padre(en este caso el master) crea las tareas y las almacena en una cola, posteriormente los hijos van cogiendo estas tareas de forma asíncrona.De esta forma se paraleliza la implementación secuencial, donde en cada iteración se hacía una única llamada a fgauss.<br>

**5. Calcula la ganancia (speedup) obtenido con la paralelización y explica los resultados obtenidos.**<br>
La ganancia está explicada en el apartado de benchmarks.<br>

**6. ¿Se te ocurre alguna optimización sobre la función fgauss? Explica qué has hecho y qué ganancia supone con respecto a la versión paralela anterior.**
