# Práctica 5: OPenMP Mandelbrot

Autor Sergio Varona Madrid

Fecha 20/12/2019

## Prefacio


Los aprendizajes que he obtenido con la realización de esta práctica han sido comprender como funciona el análisis con profiling y el análisis del rendimiento potencial con el objetivo de determinar aquellas partes del código más lentas de cómputo y las que nunca son ejecutadas.<br>
Gracias al profiling, podemos determinar la parte del código que vamos a paralelizar mediante directivas openMP.<br>
Por último, he evaluado el impacto de diversas alternativas de implementación de regiones críticas
en el rendimiento de un programa paralelo.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema
-Ejecución sobre partición hdd:

  - Linux Mint 19.2
  - Architecture:        x86_64
  - CPU op-mode(s):      32-bit, 64-bit
  - Byte Order:          Little Endian
  - CPU(s):              8
  - On-line CPU(s) list: 0-7
  - Thread(s) per core:  2
  - Core(s) per socket:  4
  - Socket(s):           1
  - NUMA node(s):        1
  - Vendor ID:           GenuineIntel
  - CPU family:          6
  - Model:               158
  - Model name:          Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz
  - Stepping:            9
  - CPU MHz:             2800.103
  - CPU max MHz:         3800,0000
  - CPU min MHz:         800,0000
  - BogoMIPS:            5616.00
  - Virtualization:      VT-x
  - L1d cache:           32K
  - L1i cache:           32K
  - L2 cache:            256K
  - L3 cache:            6144K
  - NUMA node0 CPU(s):   0-7
  - Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp


-Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i7-7700HQ
  - CPU 8 Cores: 1 socket, 2 threads/core, 4 cores/socket
  - Kernel: Linux 4.15.0-54-generic
  - CPU flags: sse, sse2, ssse3, fma, fma4 y avx
  - CPU @ 2.80GHz
  - CPU: L2: 256KiB, L3: 6144KiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre HDD

## 2. Diseño e Implementación del Software
### 2.1. Ejercicio 1.profiling
En primer lugar, se nos pide mostrar las regiones más lentas de computo, para ello usamos la herramienta profiling.<br>
Antes de nada, reducimos el parámetro n a n=1000 para facilitar el computo y agilizar las pruebas.<br>
Para realizar el profiling usamos los siguientes comandos:<br>
`gcc -g -o p5 p5.c -lm`<br>
`valgrind --tool=callgrind --dump-instr=yes ./p5`<br>
`valgrind --tool=cachegrind ./p5`<br>
`kcachegrind cachegrind.out.21128`<br>
Gracias a esto, podemos observar aquellas regiones que provocan cuello de botella y que serán las candidatas a paralelizar.

![](images/profiling.png)

En concreto, encontramos que, la función **explode**, ocupa el 73,79% del computo.Por lo que es evidente, que es la región que paralelizaremos en el ejercicio 2.

### 2.2. Ejercicio 2. Paralelización
Tal y como he mencionado en el apartado anterior, la región a paralelizar es el bucle anidado donde se produce la llamada a la función explode, que es la que más ralentiza el computo.

```
for (i = 0; i < n; i++) {
  for (j = 0; j < n; j++) {
    x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

    y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);

    count[i + j * n] = explode(x, y, count_max);
  }
}
```
La paralelización de esta parte del código, que posteriormente explicaremos quedaría de la siguiente forma:<br>

```
#pragma omp parallel private(i,j,x,y) shared(count_max,count,n,x_max,x_min,y_max,y_min)
      {
        #pragma omp for schedule(dynamic,8)
        for (i = 0; i < n; i++) {
          for (j = 0; j < n; j++) {
            x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

            y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);

            count[i + j * n] = explode(x, y, count_max);
          }
        }

```

La única directiva a explicar es **schedule(dynamic,8)** ya que al resto se han hecho referencia en prácticas anteriores.
Esta directiva lo que hace es dividir las iteraciones del bucle en bloques de tamaño 8, las cuales se asignan dinámicamente a los threads cuando van acabando su trabajo.<br>
Las etiquetas utilizadas han sido **private**, para aquellas variables que queremos hacer privadas a cada thread y que sean invisibles para los demás threads(i,j,x,y), después podía haber puesto default(shared), pero he decidido finalmente etiquetar como shared a el resto(count_max,count,n,x_max,x_min,y_max,y_min), con el objetivo de que exista una única copia de cada variable en memoria y todos los thread leen y escriben en esa dirección de memoria.<br>
Por último, indicamos cómo se realiza el paralelismo en este programa, indicando que partes son secuenciales y cuales son paralelas.La parte paralela, abarca únicamente como he mencionado arriba, los bucles anidados donde se produce la llamada a la función explode. Aquí, cada thread se divide 8 iteraciones del bucle.El resto de código(printf iniciales, creación de variables,set the image data, write an image file...) es secuencial.


### 2.3. Ejercicio 3. Sección crítica
En cuanto a la implementación de la sección crítica del siguiente código,<br>
```
c_max = 0;
for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
  }
}
```
se nos pide cuatro formas, mediante directiva openMP, mediante funciones de runtime, implementación secuencial, y con variables privadas a cada thread.<br>

**Implementación mediante directivas openMP**
```
c_max = 0;
#pragma omp for schedule(dynamic,8)
for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    #pragma omp critical
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
  }
}
```
Dividimos las tareas en 8 iteraciones para cada thread y posteriormente, cuando lleguemos a la región crítica, donde se escribe el valor c_max, usamos la directiva de openMP **critical**.Esta directiva implementa una sección crítica(todos los threads ejecutan el bloque de código, pero sólo uno puede acceder a él en cada instante de tiempo) que nos permite asegurar la exclusión mutua del bloque de código
```
if (c_max < count[i + j * n]) {
  c_max = count[i + j * n];
}
```


**Implementación mediante funciones de runtime**
```
c_max = 0;
omp_lock_t mutex;
omp_init_lock(&mutex);

for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    omp_set_lock(&mutex);
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
    omp_unset_lock(&mutex);
  }
}
omp_destroy_lock(&mutex);
```
En esta ocasión hacemos uso de la funciones de runtime, más en concreto aquellas que tienen que ver con las funciones de lock.<br>
Para ello, definimos el mutex y lo inicializamos.Posteriormente, antes de entrar en la sección crítica, el thread coge el control del lock y a la salida de esta sección lo libera.Por último, destruimos el mutex.<br>
**Implementación secuencial de esa parte de código**
```
#pragma omp single
{
  c_max = 0;
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {
        c_max = count[i + j * n];
      }
    }
  }
}
```
Ahora se nos pide una implementación secuencial, para ello, únicamente hacemos uso de la directiva **single**, para forzar a que un único thread, independientemente de cual sea, acceda a la sección crítica.<br>

**Implementación con variables privadas a cada thread y selección del máximo de todas ellas**
```
c_max = 0;
#pragma omp for schedule(dynamic,8)  reduction(max:c_max)
for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
  }
}
```
Basicamente tenemos que añadir la directiva `reduction(max:var)` para que el valor c_max sea siempre el máximo calculado por todos los hilos.El resto de la estructura es la misma

### 2.4 Makefile

Con el siguiente makefile facilito la compilación del programa para realizar pruebas y los benchmarks

```
.PHONY: build

	clean:
	rm -rf build


sec:
	mkdir -p build; \
	gcc -g -o build/p5sec src/p5omp.cpp -lm -D SECUENCIAL;

ompA:
	mkdir -p build; \
	g++ -std=c++17 -o -g -fopenmp -o build/p5ompA src/p5omp.cpp -lm -fopenmp -D OMP -D A;

ompB:
	mkdir -p build; \
	g++ -std=c++17 -o -g -fopenmp -o build/p5ompB src/p5omp.cpp -lm -fopenmp -D OMP -D B;

ompC:
	mkdir -p build; \
	g++ -std=c++17 -o -g -fopenmp -o build/p5ompC src/p5omp.cpp -lm -fopenmp -D OMP -D C;

ompD:
	mkdir -p build; \
	g++ -std=c++17 -o -g -fopenmp -o build/p5ompD src/p5omp.cpp -lm -fopenmp -D OMP -D D;

all:
  mkdir -p build; \
  gcc -g -o build/p5sec src/p5omp.cpp -lm -D SECUENCIAL;\
  g++ -std=c++17 -o -g -fopenmp -o build/p5ompA src/p5omp.cpp -lm -fopenmp -D OMP -D A;\
  g++ -std=c++17 -o -g -fopenmp -o build/p5ompB src/p5omp.cpp -lm -fopenmp -D OMP -D B;\
  g++ -std=c++17 -o -g -fopenmp -o build/p5ompC src/p5omp.cpp -lm -fopenmp -D OMP -D C;\
  g++ -std=c++17 -o -g -fopenmp -o build/p5ompD src/p5omp.cpp -lm -fopenmp -D OMP -D D;\

```



## 3. Metodología y desarrollo de las pruebas realizadas
En cuanto al benchmarking, antes de comenzar, volvemos a tener en cuenta aquellos aspectos que hemos tenido en cuenta en prácticas anteriores y que no explicaste en clase, esto es, tener el portátil conectado a la corriente, sin ningún proceso innecesario abierto, fijando el governor a userspace y fijando la misma frecuencia de los 8 cores de mi sistema.En este caso, he escogido una frecuencia mayor que en prácticas anteriores(2.8GHz), ya que en esta práctica no se pide llegar a un tiempo de ejecución determinado con la versión secuencial.<br>
Entrando más en detalle en el benchmarking, mi objetivo es representar el speedup obtenido por la cuatro implementaciones del bucle del ejercicio 3 frente al mismo bucle secuencial. He establecido n=1000 para agilizar los cálculos.<br>

En cuanto a esta primera gráfica estoy comparando la versión secuencial dada con la implementación en openMP(con la parte de sum en versión secuencial porque es la que más se asemeja a la versión original).Podemos observar, como es lógico que, aumentar el número de hilos en la versión paralela, supone una ganancia respecto a la versión secuencial, obteniendo incluso para el caso de 8 threads un speedup de casi 5.
![](images/benchmark1.png)

En esta gráfica estoy comparando las distintas implementaciones, bucle de llamada a explode + bucle del c_sum, podemos observar que, la versión implementada con directivas openMP tiene un speedup inferior al de las otras tres implementaciones.
![](images/benchmark2.png)


En esta última gráfica estoy comparando únicamente la sección crítica de cada implementación, podemos observar como la implementada con variables privada tiene una ganancia de 5 frente a la versión secuencial, en cambio, para el resto de implementaciones, proteger la sección supone una pérdida de rendimiento.
![](images/benchmark3.png)

## 4. Discusión
Para obtener los tiempos de ejecución para poder realizar los análisis he utilizado la API gettimeofday+ROI.
En cuanto a las dificultades encontradas podemos destacar ,al igual que en la práctica anterior, la complejidad del código(desde mi punto de vista), muchos bucles anidados, variables y mucho de funciones de entrada/salida.
Otro error que me ha llevado un rato, era que, al hacer el diff para ver si el código paralelo era distinto del secuencial, me mostraba que siempre eran diferentes,esto se debe a que el fichero .ppm tras la ejecución de programa secuencial y paralelo tenían distinto nombre y entonces, cuando se ejecutaba la siguiente línea de código,
```
 fprintf(file_out, "# %s created by ppma_write.c.\n", output_filename);
 ```
 lógicamente me daba que los ficheros eran distintos y como ejecutaba el diff con -q no veía que esto ocurría
