#!/usr/bin/env bash

nthreads=12
case "$1" in
    sec)
        build./p5sec 
        ;;
    omp)
        build./p5ompA 
        ;;
    runtime)
        build./p5ompB 
        ;;
    single)
        build./p5ompC 
        ;;
    priv)
        build./p5ompD 
        ;;

esac
