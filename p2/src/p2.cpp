#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/time.h>

#if COMP_SEC
void secuencial(long,double);
#endif

#if COMP_T==1
#include<thread>
#include <mutex>
std::mutex g_op;
void paralelo(double,int,int ,int ,double *);
#endif

#if COMP_OMP==1
#include <omp.h>
#endif

//Variable globales
int numThreads;
long iter;
struct timeval tf,ti;
double tiempo;


int main(int argc, char* argv[]){
	long iterations=atol(argv[1]);
	if(argc != 2 && argc !=3){
		exit(-1);
	}else{
		iter=atol(argv[1]);
		if(iter<1){
			printf("Parametro invalido");
			exit(-1);
		}
	}
	double gap=1.0/(iter);

	//Ejecución secuencial
	#if COMP_SEC == 1
	printf("Ejecucion secuencial \n");
	gettimeofday(&ti,NULL);
	secuencial(iter,gap);
	gettimeofday(&tf,NULL);
	tiempo = (tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
	printf("Tiempo empleado secuencial: %f ms\n", tiempo);
	#endif


	#if COMP_T == 1
	printf("Ejecucion paralela \n");
	int i,tareas,tareasRestantes;
	double resultado=0;

	if(getenv("NUM_THREADS")){
		numThreads=atoi(getenv("NUM_THREADS"));
	}else{
		numThreads=atoi(argv[2]);
	}
	if(iter<numThreads){
		numThreads=iter;
	}
	tareas=iter/numThreads;
	tareasRestantes=iter%numThreads;
	std::thread threads[numThreads];

	gettimeofday(&ti,NULL);
	for(i=0; i<numThreads;i++){
		int posThread=i*tareas;
		threads[i]=std::thread(paralelo,gap,tareas,posThread,tareasRestantes,&resultado);
	}
	for(i=0 ; i<numThreads ; i++){
		threads[i].join();
	}

	gettimeofday(&tf,NULL);
	tiempo = (tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
	printf("Tiempo empleado multi-thread: %f ms\n", tiempo);
	printf("ellipse: %f \n", resultado*2*gap/sqrt((4*1.5*1-2.4*2.4)));
	#endif

	#if COMP_OMP == 1
	printf("Ejecucion omp \n");
	if(getenv("NUM_THREADS")){
		numThreads=atoi(getenv("NUM_THREADS"));
	}else{
		numThreads=atoi(argv[2]);
	}

	int i;
	double ellipse=0;
	omp_set_num_threads(numThreads);
	gettimeofday(&ti,NULL);
	#pragma omp parallel shared(gap,iter) private (i) reduction(+:ellipse)
	#pragma omp for
	for(i=0;i<iter;i++){
		ellipse+=4/(1+((i+0.5)*gap)*((i+0.5)*gap));
	}
	gettimeofday(&tf,NULL);
	tiempo = (tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
	printf("Tiempo empleado openMP: %f ms\n", tiempo);

	printf("ellipse: %f \n", ellipse*2*gap/sqrt((4*1.5*1-2.4*2.4)));
	#endif
	return 0;
}


///////////////////////////FUNCIONES/////////////////////////////////////
////////////////////////////////////////////////////////////////////////
#if COMP_T==1
void paralelo(double gap,int tareas,int posThread,int tareasRestantes,double *resultado){
	int i;
	double ellipse=0;
	for(i=posThread;i<posThread+tareas;i++){
		ellipse+=4/(1+((i+0.5)*gap)*((i+0.5)*gap));
	}
  std::lock_guard<std::mutex> l(g_op);
	{
	*resultado+=ellipse;
	}
}
#endif

#if COMP_SEC==1
void secuencial(long iter,double gap){
	int i=0;
	double tmp=0;
	double ellipse=0;
	for (i=0; i<iter; i++) {
		ellipse+=4/(1+((i+0.5)*gap)*((i+0.5)*gap));
	}
	printf("ellipse: %f \n", ellipse*2*gap/sqrt((4*1.5*1-2.4*2.4)));
}
#endif
