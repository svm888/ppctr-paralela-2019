# P2: Optimizing
Eduardo Barañano Canales y Sergio Varona Madrid

16/12/2019

## Prefacio
>Hay una serie de puntos que nos gustaría destacar de la práctica realizada. En primer lugar, aprendimos a realizar la compilación condicional para diferenciar la parte secuencial, multithread y con openMP, apoyándonos en el fichero makefile, algo que no habíamos realizado nunca antes, y que nos ha parecido bastante útil para tener partes muy bien diferenciadas en el programa y no mezclar demasiado el código. Incluyendo variables de preprocesador y activándolas con = 1, podemos delimitar que parte del código queremos que se compile cada vez a través del makefile proporcionado. Por otra parte, se nos han introducido también el uso de las variables de entorno, en este caso para poder elegir el número de threads sin necesidad de pasarlo por la consola cada vez que hacemos una ejecución. Bastan con definir la variable entorno desde el terminal (por ejemplo, en este caso, NUM_THREADS), y a continuación utilizar el comando export para que después el propio código que hemos programado detecte el uso de dicha variable y no tengamos que pasarla más hasta que cerremos la consola. Además de todo esto, hemos reforzado aún más la visión que teníamos de la diferencia entre ejecución secuencial y multihilo, dándonos cuenta de que para un pequeño número de iteraciones sigue siendo más óptimo lanzar el programa con la parte secuencial (y más en el caso de esta práctica, en la que únicamente se produce un cálculo matemático sencillo, a diferencia de la anterior en la que teníamos que particionar un array y calcular su resto, y eso provocaba un mayor esfuerzo para el terminal). Al hilo de lo que acabamos de comentar, también hemos aprendido que para programas con cálculos sencillos como este, la utilización de las cláusulas OpenMP (en este caso únicamente hemos usado parallel y for) nos permite realizar la programación multihilo de una forma mucho más sencilla y rápida que desplegando hilos manualmente como hicimos en la práctica anterior y también hemos hecho en esta.

## Índice

1. Sistema
2. Diseño e implementación del software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema
La descripción del sistema donde se realiza el benchmarking es la siguiente:

-Ejecución sobre partición hdd:
  - Linux Mint 19.2
  - Architecture:        x86_64
  - CPU op-mode(s):      32-bit, 64-bit
  - Byte Order:          Little Endian
  - CPU(s):              8
  - On-line CPU(s) list: 0-7
  - Thread(s) per core:  2
  - Core(s) per socket:  4
  - Socket(s):           1
  - NUMA node(s):        1
  - Vendor ID:           GenuineIntel
  - CPU family:          6
  - Model:               158
  - Model name:          Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz
  - Stepping:            9
  - CPU MHz:             800,0000
  - CPU max MHz:         800,0000
  - CPU min MHz:         800,0000
  - BogoMIPS:            5616.00
  - Virtualization:      VT-x
  - L1d cache:           32K
  - L1i cache:           32K
  - L2 cache:            256K
  - L3 cache:            6144K
  - NUMA node0 CPU(s):   0-7
  - Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp


-Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i7-7700HQ
  - CPU 8 Cores: 1 socket, 2 threads/core, 4 cores/socket
  - Kernel: Linux 4.15.0-54-generic
  - CPU flags: sse, sse2, ssse3, fma, fma4 y avx
  - CPU @ 2.80GHz
  - CPU: L2: 256KiB, L3: 6144KiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre HDD

## 2. Diseño e implementación del Software
En esta práctica, se nos pide realizar tanto una implementación multihilo normal igual que la programada en la práctica anterior, como otra utilizando las cláusulas proporcionadas por el estándar OpenMP. A continuación, procederemos a describir la implementación de nuestro código, comenzando por la parte de multithreading básico:

En primer lugar, nos gustaría comentar la utilización de la variable de entorno NUM_THREADS para obtener el número de hilos a utilizar a través de esta si no queremos pasar siempre dicho número por consola. Esto se realiza en esta parte del código (tanto en la zona de multithreading como en la de OpenMP):
~~~
if(getenv("NUM_THREADS")){
  numThreads=atoi(getenv("NUM_THREADS"));
}else{
  numThreads=atoi(argv[2]);
}
~~~
Como se puede observar, si el programa detecta que se ha declarado una variable de entorno en el terminal, obtendremos el valor (en este caso del número de hilos) a través de getenv. En caso contrario, realizaremos el parseo como hemos hecho habitualmente, utilizando argv, en este caso argv[2], ya que como sabemos argv[0] es el nombre del fichero binario ejecutable, y argv[1] el número de iteraciones que utilizaremos para el cálculo del valor de la elipse.

A continuación, vamos a explicar la paralelización realizada para la parte multihilo. De forma muy similar a la acontecida en la práctica previa, calculamos la cota, que es el número de iteraciones que realizará cada hilo. También realizamos en primera instancia el cálculo de las iteraciones restantes a realizar al final, pero como se observa, no es necesario para realizar el cálculo de la elipse, ya que en caso de sobrar iteraciones, la cantidad suele ser despreciable y no afecta al cálculo final de la medida de la elipse por parte de los hilos. A continuación, realizamos el despliegue y sincronización de los hilos creados de la siguiente forma:
~~~
for(i=0; i<numThreads;i++){
  int posThread=i*tareas;
  threads[i]=std::thread(paralelo,gap,tareas,posThread,tareasRestantes,&resultado);
}

for(i=0 ; i<numThreads ; i++){
  threads[i].join();
}
~~~
Con el bucle for inicial, nos aseguramos de que cada hilo entre una vez a la función paralelo, a la que le pasamos gap, que es un parámetro necesario para hacer el cálculo matemático requerido. Además, le pasamos las tareas (nº de iteraciones a realizar por cada thread), la posición del thread, que nos sirve para controlar que parte tiene que calcular cada hilo, las tareasRestantes, que como hemos dicho, finalmente despreciamos en el cálculo (no es necesario pasarlas como parámetro), y, por último, un puntero a resultado, que es la variable donde almacenamos el resultado final que luego pintaremos. En el segundo bucle for, únicamente llamamos a join para sincronizar los hilos en ese punto del programa y asegurarnos de que no ha habido ningún error extraño en el cómputo de la operación que se realiza en la función.

Una vez descrita la llamada, vamos a describir la función que realiza el cálculo de la elipse:
~~~
void paralelo(double gap,int tareas,int posThread,int tareasRestantes,double *resultado){
	int i;
	double ellipse=0;

	for(i=posThread;i<posThread+tareas;i++){
		ellipse+=4/(1+((i+0.5)*gap)*((i+0.5)*gap));
	}

  std::lock_guard<std::mutex> l(g_op);
	{
	*resultado+=ellipse;
	}
}
~~~
En esta función, cada hilo calcula su parte, esto lo delimitamos con la variable posThread, desde la que parte cada hilo, para llegar hasta posThread + tareas, asegurándonos de que cada hilo está realizando el cálculo de nuevas iteraciones. Una vez hecho esto, en el interior del for únicamente volcamos en la variable ellipse el cálculo de esta. Por último, acumulamos en el puntero resultado el resultado de ellipse, protegiéndolo previamente con el mutex, para evitar que los hilos no respeten la zona de exclusión mútua y se escriban los datos en la variable a la vez por varios hilos, lo que podría desembocar en cálculos erróneos.

Ya hemos explicado la implementación de la zona multithread del código. Como vemos, es una especie de simplificación de lo realizado en la práctica previa, ya que es más sencillo paralelizar un cálculo matemático que una tarea en la que tenemos que particionar un array primero para luego acumular los resultados iterando previamente. Además, ayuda el no tener en cuenta el resto, que como hemos comentado, despreciamos.

Ahora, trataremos la implementación de la zona programada bajo el estándar OpenMP. A continuación, se expone la parte del código con las cláusulas utilizadas para paralelizar nuestra operación matemática del cálculo de la elipse.
~~~
#pragma omp parallel shared(gap,iter) private (i) reduction(+:ellipse)
#pragma omp for
for(i=0;i<iter;i++){
  ellipse+=4/(1+((i+0.5)*gap)*((i+0.5)*gap));
}
~~~
Como se observa, para paralelizar esta función, hemos añadido una cláusula parallel, que lo que hace es desplegar los hilos necesarios para acometer la paralelización. En este caso, no especificamos cual es el nº de hilos que queremos utilizar, así que lo determina el propio programa en función de diferentes parámetros internos. Además de eso, especificamos que variables son compartidas durante el cómputo (el número de iteraciones iter y el dato gap), y cual es privada para cada hilo (el índice i del bucle). Por último, añadimos reduction con el operador + para la variable ellipse, esto hace que el resultado parcial calculado por cada hilo se acumule en forma de suma en la variable ellipse, que es la que pintaremos finalmente para observar cual es nuestro resultado. También añadimos a posteriori la cláusula for, necesaria ya que esta es la que especifica el reparto de las iteraciones del bucle del cálculo entre los diferentes hilos. Con esto, logramos de una forma muy sencilla un resultado temporal similar al logrado en la parte multithread, pero con la ventaja de que el código empleado utilizando únicamente las dos cláusulas parallel y for es muy ligero y ocupa muy poco espacio en nuestro programa.

Con esto, quedaría descrita la implementación del código escrito para la realización de la práctica 2. Ahora, en el apartado 3, explicaremos las pruebas realizadas y la metodología utilizada en el cálculo de los diferentes benchmarks.

## 3. Metodología y desarrollo de las pruebas realizadas
Antes de realizar los benchmarks hemos seguido las indicaciones que nos diste en clase, tener el portátil conectado a la corriente, sin ninguna aplicación o proceso en funcionamiento y recién encendido.Además, de fijar la frecuencia de procesador y hacer unas ejecuciones de calentamiento, para preparar el pc. En esta práctica hemos decidido establecer una frecuencia para los 8 cores de 800Mhz.
En cuanto a los benchmarks, hemos decidido hacer las ejecuciones con un número de iteraciones que, en nuestro secuencial, nos han dado un tiempo de 1,5,8 y 10 segundos (20.000.000,125.000.000,210.000.000,265.000.000 iteraciones respectivamente).Además, hemos usado los scripts proporcionados, benchmark.sh y benchsuite.sh para conseguir los tiempos de ejecución y poder calcular así los speedup.

Entrando más en el desarrollo de los benchmarks, hemos decidido hacer 4, uno comparando los speedups secuencial y multihilo, otro secuencial y openMP, otro comparando el speedup del multihilo y openMP respecto al secuencial y por último, uno comparando el speedup de los 3 para 265.000.000 iteraciones.

En cuanto a la primera gráfica, estamos comparando el speedup secuencial y multihilo, como podemos observar, el speedup general de 2 a 8 hilos es menor según aumentan el número de iteraciones, esto se debe a que la diferencia tiempos de ejecución del secuencial-paralelo son menores a medida que aumenta el número de iteraciones.
![](images/benchmark1.png)
Podemos observar a su vez, que para este primera gráfica, obtenemos, como es lógico, un speedup superior para una ejecución con un número mayor de hilos independientemente del número de iteraciones.


En cuanto a la segunda gráfica, estamos comparando la ejecución secuencial con la versión de openMP, podemos observar que los speedups son prácticamente idénticos a la versión paralela, tal y como explicaremos en la tercer benchmark.
![](images/benchmark2.png)
Observamos que ocurre lo mismo,el speedup general de 2 a 8 hilos es menor según aumentan el número de iteraciones.

Hemos decidido crear otras gráficas para aclarar que la versión multihilo y la implementada en openMP tienen unos speedups casi idénticos para cualquier combinación de *x* número de hilos e *y* iteraciones.Esto se debe a que ambas implementaciones tienen las mismas optimizaciones respecto del secuencial.
![](images/benchmark3.png)




En cuanto a esta última gráfica, hemos querido reflejar la mejora obtenida y el correcto resultado que se obtiene al implementar tanto multihilo o openMP.Por ejemplo, para el número de iteraciones indicado, observamos que hay una gran mejora en cuanto a los speedup y a medida que aumenta el número de hilos(hasta 8) esta mejora es mas considerable.
![](images/benchmark4.png)

## 4. Discusión
Como dificultades principales a destacar, podemos mencionar 2. La primera, que en el cálculo de la paralelización realizado con OpenMP, el programa no nos estaba arrojando el mismo resultado de la parte secuencial y de la multithread normal. Tras depurar el código y realizar diferentes pruebas, observamos que el problema estaba en que habíamos declarado la cláusula parallel, pero no habíamos incluido la cláusula for, por lo que el programa se estaba volviendo loco al intentar repartir las iteraciones entre los hilos y el cálculo final daba valores mucho mayores al correcto que no tenían absolutamente nada que ver con este.

Por otra parte, destacar también que al realizar la parte multithread, se nos olvidó incluir el mutex en la parte de exclusión mútua, es decir, en el puntero común a los hilos donde acumulamos el resultado parcial obtenido por cada thread. En principio, en las ejecuciones que habíamos realizado, no habíamos tenido ningún problema (es decir, la no inclusión del mutex en el código no nos estaba provocando errores a la hora de arrojar los resultados), pero al realizar la memoria y explicar la implementación nos dimos cuenta de que era un error no utilizar el mutex, por lo que rapidamente corregimos el código.

Antes de comentar todo esto, mencionar que se ha utilizado la api gettimeofday + ROI para calcular los tiempos de ejecución.

A continuación, hablaremos de las optimizaciones realizadas en la parte multithread y OpenMP respecto al cálculo secuencial.

En el código multithread, hemos seguido una pauta muy parecida a la realizada en la práctica anterior, como ya hemos comentado en varias ocasiones durante la realización de este documento. Hemos decidido únicamente repartir el número de iteraciones a realizar para aproximar el cálculo de la elipse entre nuestros hilos, sin mayores florituras, encargándose cada thread de su parte correspondiente, todo esto respecto de nuestro secuencial.

Respecto del secuencial propuesto en la práctica, hemos eliminado la función attach, que lo único que hacía era rellenar el struct agreggator y pasarselo a la sección agreggator del struct data. Todo esto era inservible, ya que es más eficiente directamente declarar las variables, prescindir de A, B y C (podemos usarlas como números directamente en lugar de variables) y eliminar la función parse que rellenaba el struct data, ya que como acabamos de mencionar, no lo utilizaremos, si no que pasaremos directamente los datos como el número de iteraciones o gap directamente a nuestra función secuencial. Añadir también que, como consecuencia de esto, hemos eliminado el fichero p2.hpp, que era el que contenía las definiciones de los structs data y agreggator. Con esto nos damos cuenta de que la función secuencial estaba declara de forma muy errática, y realizando un montón de pasos innecesarios que únicamente hacían de este código algo poco eficiente.

Además, hemos tomado la decisión de utilizar mutex en lugar de variables atómicas, ya que quedó bastante claro que el uso de estas en sustitución del mutex no se ve reflejado en un ahorro de tiempo a la hora de realizar las ejecuciones en el programa.

La optimización realizada por parte de la implementación OpenMP es muy sencilla, ya que únicamente hemos incluido las dos cláusulas estrictamente necesarias para que el código se comportase de forma correcta, estas son, como se mencionó previamente, parallel y for, con sus respectivas directivas ya descritas.

## 5. Propuestas optativas
No se han realizado las partes opcionales propuestas en la práctica
