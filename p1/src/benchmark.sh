#!/usr/bin/env bash
file=resultado4.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2 st3 mt3 st4 mt4 st5 mt5; do
        echo $benchcase >> $file
        for i in `seq 1 1 2`;
        do
            printf "$i:" >> $file # iteration
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
