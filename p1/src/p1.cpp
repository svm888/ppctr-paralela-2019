#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#define DEBUG 1
#define FEATURE_LOGGER 0
#define FEATURE_OPTIMIZE 1


#if FEATURE_LOGGER==1
//Variable condicional global
std::condition_variable g_cond;
bool listo =false;
double *resTotal;
//Numero de threads que han terminado
int contador=0;
#endif


#if FEATURE_OPTIMIZE==1
//Creamos la variable atomica
std::atomic<double>va;
#endif

//Prototipos de funciones
void paralelo(int,int,int, double*,int,int,int,double*,int);
void logger(int ,int,double *,std::condition_variable *,bool *);

bool hayResto=false;
//Mutex para controlar la operacion
std::mutex g_op;
//Mutex para controlare el resto en caso en el que lo haya
std::mutex g_resto;
//Mutex para controlar que los resultados de los threads se pasen de uno en uno
std::mutex g_m;
int resto=0;
int eleRestantes=0;

int main( int argc, char* argv[]){

  //En caso de recibir un numero de argumentos invalidos
  if(argc!=3 && argc!=5){
    printf("Numero de argumentos invalidos \n");
    exit(-1);
  }
  //Variables para calcular el tiempo de ejecucion
  struct timeval ti,tf;
  double tiempo;

  int numHilos;
  //Ejecucion paralela
    numHilos=atoi(argv[4]);
    if(numHilos>12){
      printf("Numero maximo de threads alcanzado \n");
      exit(-1);
    }

  //Solucion de la operacion
  double solucion=0;
  //Array a utilizar
  int numEle=atoi(argv[1]);

  //Obtenemos el tipo de operacion
  char *tipoOperacion=argv[2];

  //Obtenemos el espacio en memoria necesario
  double* arrayOperaciones=(double*)malloc(sizeof(double) * numEle);

  //Rellenamos el array
  for(int i=0;i<numEle;i++){
    arrayOperaciones[i]=i;
  }

  #if DEBUG==1
    printf("Tipo operación: %s\n",tipoOperacion );
    printf("Número de elementos:%d\n",numEle );
  #endif

  int op;
  if(strcmp(tipoOperacion,"sum")==0){
    op=0;
  }else if(strcmp(tipoOperacion,"sub")==0){
    op=1;
  }else{
    op=2;
  }

  if(argc==3){
    /*
    *								*
    PARTE SECUENCIAL
    *								*
    */

    gettimeofday(&ti,NULL);
    //Si el tipo de operación es suma
    if(op==0){
      for(int i=0;i<numEle;i++){
        solucion=solucion+arrayOperaciones[i];
      }
    }
    //Si el tipo de operación es resta
    if(op==1){
      for(int i=0;i<numEle;i++){
        solucion=solucion-arrayOperaciones[i];
      }
    }
    //Si el tipo de operación es xor
    if(op==2){
      for(int i=0;i<numEle;i++){
        solucion=(int)solucion^(int)arrayOperaciones[i];
      }
    }

    //Termina la ejecución del secuencial
    gettimeofday(&tf,NULL);
    tiempo = (tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
    #if DEBUG ==1
    printf("\n");
    printf("///////////SECUENCIAL////////////\n");
    printf("Tiempo empleado secuencial: %f ms\n", tiempo);
    printf("Resultado de las operaciones: %f\n",solucion);
    #endif

}

    if(argc==5){
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    /*
    *								*
    PARTE MULTI-THREAD
    *								*
    */
    //thread
    solucion=0;
    std::thread threads[numHilos];

    //Tamaño del subvector que se llevará cada hilo
    //El tamaño indica el numero de elementos que se llevara cada thread
    int tam=numEle/numHilos;

    if(numEle%numHilos!=0){
      eleRestantes=numEle-tam*numHilos;
      resto=arrayOperaciones[numEle-tam];
      hayResto=true;
    }

    //Si el numero de elementos es mayor que el numero de hilos solo
    //necesitamos un numero de hilos igual al numero de elementos
    if(numEle<numHilos){
      numHilos=numEle;
    }


    #if DEBUG==1
    printf("\n");
    printf("///////////MULTI HILO////////////\n");
    printf("Cada thread se encarga de %d elementos\n",tam);
    printf("Elementos restantes: %d\n",eleRestantes);
    #endif



    #if FEATURE_LOGGER==1
    //Creamos la variable condicional
    std::condition_variable cv;
    //Hilo del logger
    std::thread log;
    //Resultado del logger
    double resLogger=0;
    double arrayLogger[numHilos];
    resTotal=arrayLogger;
    //True si ha terminado el logger
    bool finLog=false;
    //Llamamos a la función del logger
    log=std::thread(logger,numHilos,op,&resLogger,&cv,&finLog);
    #endif

    //Empieza el tiempo de ejecucion
    gettimeofday(&ti,NULL);
    for(int i=0; i<numHilos;i++){
      //Calculamos la posicion del array perteneciente a cada thread con el que le tocara trabajar
      int posThread=i*tam;
      threads[i]=std::thread(paralelo,op,posThread,resto,arrayOperaciones,tam,numHilos,numEle,&solucion,i);
    }

    for(int i=0; i<numHilos;i++){
      threads[i].join();
    }

    #if FEATURE_LOGGER==1
    std::unique_lock<std::mutex> lm(g_m);
    cv1.wait(lm, [&finLog] {return finLog;});
    log.join();
    lm.unlock();
    #if DEBUG==1
    //Si la solución paralela es igual que la del logger esta correcto
    if(solucion==resLogger){
      printf("Logger correcto.Resultado logger: %f,Resultado threads:%f\n ",resLogger,solucion);
    }else{
      //La solucion del logger es distinta que la de la ejecucion paralela
      printf("Logger incorrecto.Resultado logger: %f,Resultado threads:%f\n",resLogger,solucion);
    }
    #endif
    #endif
    //La ejecucion termina, por lo que se para el tiempo
    gettimeofday(&tf,NULL);
    tiempo = (tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
    free(arrayOperaciones);

    #if FEATURE_OPTIMIZE==0
    #if DEBUG ==1
    printf("Tiempo empleado multi-thread: %f ms\n", tiempo);
    printf("Resultado de las operaciones: %f\n",solucion);
    #endif
    #endif


    #if FEATURE_OPTIMIZE==1
    double copia=va;
    #if DEBUG ==1
      //Tiempos y resultados para la versión optimizada
    printf("Se ha hecho uso del modo optimizado\n");
    printf("Tiempo empleado multi-thread optimizado: %f ms\n", tiempo);
    printf("Resultado optimizado: %f\n",copia);
    #endif
    #endif

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    return 0;

  }
}


///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//FUNCIONES//
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

#if FEATURE_LOGGER
/*
*Función que se encarga de obtener los resultados de cada hilo e ir sumandolos.
*Sumará y notificara el resultado gracias a otra variable global
*
*/
void logger(int numHilos,int op,double *resLogger,std::condition_variable *cv,bool *finLog){
  double resultado=0;
  double arrayLogger[numHilos];
  int i;
  //Hasta que no acaben todos los hilos
  while(contador !=numHilos){
    //El hilo actual espera el mutex y espera
    std::unique_lock<std::mutex>lock(g_m);
    g_cond.wait(lock,[]{return listo;});
    listo=false;
    //Liberamos el mutex
    lock.unlock();
  }
  for(i; i<numHilos;i++){
    arrayLogger[i]=resTotal[i];
  }
  //Mostramos los resultados pertenecientes a cada hilo
  for(i=0;i<numHilos;i++){
    printf("El thread %d tiene como resultado del log:%lf.\n", i, arrayLogger[i]);
  }
  //Calculamos el resultado total
  for(i=0;i<numHilos;i++){
    if(op==0){
      resultado=resultado+arrayLogger[i];
    }
    if(op==1){
      resultado=resultado-arrayLogger[i];
    }
    if(op==2){
      resultado=(int)resultado^(int)arrayLogger[i];
    }
  }
  //Mostramos el resultado del logger
  #if DEBUG==1
  printf("Resultado logger:%f\n",resultado);
  #endif
  //Lo guardamos en la variable global
  *resLogger=resultado;
  //Hemos terminado
  *finLog=true;
  //Notificamos
  cv->notify_one();
}
#endif

/**
*Función que se encarga de calcular la parte los elementos que pertenecen a cada hilo
*/
void paralelo(int op,int posThread,int resto, double* vector,int tam,int numHilos,int numEle,double* resultado,int idThread){
  //Resultado que obtendrá cada hilo y que se sumará al resultado total
  long resParcial=0;
  //El bucle recorre desde la primera posicion del array perteneciente al hilo actual
  // mas el tamaño del subarray (numEle/numHilos), es decir hasta tam+posThread
  for(int i=posThread;i<posThread+tam;i++){
    if(op==0){
      resParcial+=(long)vector[i];
    }
    //Si el tipo de operación es resta
    if(op==1){
      resParcial-=(long)vector[i];
    }
    //Si el tipo de operación es xor
    if(op==2){
      resParcial=(int)resParcial^(int)vector[i];
    }
  }

  //Si hay resto el primer thread que termina se encarga de la parte sobrante
  if (hayResto){
    std::lock_guard<std::mutex>guard(g_resto);
    if(op==0){
      resParcial=resParcial+resto;
    }
    if(op==1){
      resParcial=resParcial-resto;
    }
    if(op==2){
      resParcial=(int)resParcial^(int)resto;
    }
    hayResto=false;
    resto=0;
  }
  #if FEATURE_OPTIMIZE==0
  std::lock_guard<std::mutex>guard(g_op);
  #if FEATURE_LOGGER ==1
  resTotal[idThread]=resParcial;
  contador++;
  if(!listo){
    listo=true;
  }
  g_cond.notify_one();
  #endif

  if(op==0 || op==1){
    *resultado+=resParcial;
  }else{
    *resultado=(int)*resultado^(int)resParcial;
  }
  #endif

  #if FEATURE_OPTIMIZE==1
  if(op==0 || op==1){
    va=va+resParcial;
  }else{
    va=(int)va^(int)resParcial;
  }
  #endif

}
