#!/usr/bin/env bash
size1=1000
size2=100000
size3=10000000
size4=100000000
size5=500000000
op=sum
nthreads=12
case "$1" in
    st1)
        ./p1 $size1 $op
        ;;
    mt1)
        ./p1 $size1 $op --multi-thread $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op --multi-thread $nthreads
        ;;
    st3)
        ./p1 $size3 $op
        ;;
    mt3)
        ./p1 $size3 $op --multi-thread $nthreads
        ;;
    st4)
        ./p1 $size4 $op
        ;;
    mt4)
        ./p1 $size4 $op --multi-thread $nthreads
        ;;
    st5)
        ./p1 $size5 $op
        ;;
    mt5)
        ./p1 $size5 $op --multi-thread $nthreads
        ;;

esac
