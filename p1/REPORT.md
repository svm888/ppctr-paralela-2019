# P1: Multithreading en C++

Autor: Sergio Varona Madrid

Fecha 11/12/2019

## Prefacio

La práctica me ha servido principalmente para recordar lo fundamental de C aprendido en asignaturas anteriores, sobretodo, aquellos conceptos que se nos hacían más difíciles como bien puede ser el manejo de punteros, reservar memoria, obtener los argumentos que se pasan por parámetros o trabajar con variables de preprocesador. En cuanto al apartado de la paralelización la práctica te enseña la gran mejora de rendimiento que puedes conseguir en una ejecución utilizando threads respecto de una implementación secuencial para el mismo problema, así como en que momentos tiene lugar la sección crítica y es necesario introducir un mutex para controlar que solo un proceso entre a esta sección.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas


## 1. Sistema

-Ejecución sobre partición hdd:

  - Linux Mint 19.2
  - Architecture:        x86_64
  - CPU op-mode(s):      32-bit, 64-bit
  - Byte Order:          Little Endian
  - CPU(s):              8
  - On-line CPU(s) list: 0-7
  - Thread(s) per core:  2
  - Core(s) per socket:  4
  - Socket(s):           1
  - NUMA node(s):        1
  - Vendor ID:           GenuineIntel
  - CPU family:          6
  - Model:               158
  - Model name:          Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz
  - Stepping:            9
  - CPU MHz:             2200.103
  - CPU max MHz:         3800,0000
  - CPU min MHz:         800,0000
  - BogoMIPS:            5616.00
  - Virtualization:      VT-x
  - L1d cache:           32K
  - L1i cache:           32K
  - L2 cache:            256K
  - L3 cache:            6144K
  - NUMA node0 CPU(s):   0-7
  - Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp


-Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i7-7700HQ
  - CPU 8 Cores: 1 socket, 2 threads/core, 4 cores/socket
  - Kernel: Linux 4.15.0-54-generic
  - CPU flags: sse, sse2, ssse3, fma, fma4 y avx
  - CPU @ 2.80GHz
  - CPU: L2: 256KiB, L3: 6144KiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre HDD


## 2. Diseño e Implementación del Software

En cuanto al diseño e implementación, primero se pedía desarrollar el **caso base**, el cual se centra en implementar un programa para hacer cálculos con tres operaciones ((sum|sub|xor)) , admitiendo para ello dos modos de funcionamiento, el secuencial y el multi-thread. Para el primero el programa unicamente recorrerá un array con los valores almacenados con los que hay que operar y guardará la solución en una variable que se mostrará posteriormente en la terminal. En el caso de la implementación con hilos, hay que conseguir que todos se lleven el mismo trabajo,y en caso de no ser una operación exacta, dejar el resto al primer hilo que termine.


Entrando más en detalle con la **parte secuencial**, para el cálculo, creamos un array de tamaño *numEle*, el cual rellenamos posteriormente de forma incremental desde 0 hasta *numEle* iteraciones,

```
  double array[numEle];
  for(int i=0;i<numEle;i++){
    array[i]=(double)i;
  }
```

tras esto, comenzamos a contar el tiempo de ejecución con la **API gettimofday** y en función del parámetro obtenido como tipo de operación haremos una operación o otra donde basicamente recorreremos el array y sumaremos(en el caso de la suma) el valor de elemento i al valor total,

```
gettimeofday(&ti,NULL);
  if(op==0){
    for(int i=0;i<numEle;i++){
      solucion=solucion+array[i];
    }
  }
  //Resto de operaciones posibles
  ...

```


 tras esto, paramos de contar el tiempo y mostramos los resultados(tiempo de ejecución y resultado de las operaciones).
```
 gettimeofday(&tf,NULL);
 tiempo = (double)(tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
 ```




En cuanto a la **parte multithreading**, tenemos que tener en cuenta primeramente en que momento vamos a necesitar acceder a la sección crítica, esto sucede cuando accedemos al resultado total cuando los hilos almacenan su resultado parcial y cuando el primer hilo debe calcular el resto en caso de haberlo. Necesitaremos además reservar un espacio de memoria equivalente al tamaño del número de elementos por el número de bytes que ocupa cada uno.
```
//Obtenemos el espacio en memoria necesario
double* arrayOperaciones=(double*)malloc(sizeof(double) * numEle);

```

Posteriormente, calculamos el resto, rellenaremos el array y crearemos un número de hilos igual al argv=atoi(argv[4]).

```

  solucion=0;
  std::thread threads[numHilos];

  ...

  int tam=numEle/numHilos;
  if(numEle%numHilos!=0){
    eleRestantes=numEle-tam*numHilos;
    resto=arrayOperaciones[numEle-tam];
    hayResto=true;
  }

  if(numEle<numHilos){
    numHilos=numEle;
  }
```

En este momento haremos la llamada a la función encargada de realizar la función correspondiente por lo que será necesario volver a usar la funcion gettimeofday de la **API gettimeofday**. La función correspondiente a cada operación se encarga de, en un primer momento, al igual que en el caso secuencial, calcular el resultado, pero en este caso el perteneciente al subvector del que se encarga cada hilo, tras esto, en el caso de haber resto, el primer hilo toma el mutex, lo calcula y lo añade a su resultado parcial que posteriormente se añadirá al resultado global tomando para ello el mutex correspondiente para evitar problemas a la hora de acceder a esta sección crítica.
Para implementar la parte del resto lo he hecho de la siguiente forma:


```
if (hayResto){
  std::lock_guard<std::mutex>guard(g_resto);
  if(op==0){
    resParcial=resParcial+resto;
  }
  //Resto de operaciones posibles
  ...

  hayResto=false;
  resto=0;
}
```
Donde una variable controla nos indica si hay resto o no, en caso de haberlo, el primero en terminar toma el mutex, calcula el resto y se lo pasa al resultado parcial para ese thread, que posteriormente será añadido al global.





En cuanto al **FEATURE_LOGGER**, el objetivo de esta parte hera crear un hilo "logger" para ir obteniendo los resultados de cada hilo y sumándolos él mismo, sin afectar al rendimiento en el resto de las secciones en caso de desactivarlo.
Para ello, creamos la variables globales necesarias, como son la variable condicional, el resultado total, el contador del numero de threads( nos indicará cuando el logger tenga que acabar).
```
#if FEATURE_LOGGER==1
std::condition_variable g_cond;
bool listo =false;
double *resTotal;
int contador=0;
#endif
```
Hay que tener en cuenta que no tiene que afectar al rendimiento en la ejecución como he dicho anteriormente, por lo que tiene que ir entre variables de preprocesador.<br>
Poteriormente, en el main, en la parte de la ejecución paralela, creamos la variables necesarias para hacer la llamada a la función.<br>
Esta función esperará a que todos los hilos terminen, y cuando esto ocurra, almacenará el resultado en el array creado.

```
while(contador !=numHilos){
  std::unique_lock<std::mutex>lock(g_m);
  g_cond.wait(lock,[]{return listo;});
  listo=false;
  lock.unlock();
}
for(i; i<numHilos;i++){
  arrayLogger[i]=resTotal[i];
}
```
Luego, muestra el resultado individualmente gracias a al array que hemos creado y por último, los suma todo y lo muestra.<br>
Finalmente, antes de terminar en el main, comprueba si el resultado obtenido por el logger, es el mismo que obtenido por la ejecución multi-thread sin logger.
```
if(solucion==resLogger){
    printf("Logger correcto.Resultado logger: %f,Resultado threads:%f\n ",resLogger,solucion);
  }else{
    //La solucion del logger es distinta que la de la ejecucion paralela
    printf("Logger incorrecto.Resultado logger: %f,Resultado threads:%f\n",resLogger,solucion);
  }
```

Hasta aquí estaría la parte base y el logger, en cuanto al **FEATURE_OPTIMIZE**, el método para mejorarlo es usar variables atómicas como se explicó en clase.Esto se debe a que las variables átomicas tienen un comportamiento especial y no necesitan ser protegidas por un mutex ya que no se puede producir un data race(dos threads intentan acceder a la misma variable al mismo tiempo) gracias a su comportamiento.<br>
Creamos la variable atómica global

```
#include <atomic>

...

#if FEATURE_OPTIMIZE==1
std::atomic<double>va;
#endif
```
Y dentro de la función que se encarga de calcular el resultado parcial de cada hilo y sumarlo, lo hacemos con variables atómicas, por lo que evitamos depender del mutex.<br>

```
#if FEATURE_OPTIMIZE==0
std::lock_guard<std::mutex>guard(g_op);

...

#endif

#if FEATURE_OPTIMIZE==1
if(op==0 || op==1){
  va=va+resParcial;
}else{
  va=(int)va^(int)resParcial;
}
#endif
```

Por último, queda hablar del **conector con java**, el objetivo de esto era modularizar el código a modo de librería para ejecutar el código Java con la porción extraída de C.
En esta parte, he tenido bastantes problemas, el primero de ellos, no conseguía establece la variable de entorno JAVA_HOME como mi directorio a la carpeta de java, por lo que he tenido que poner la ruta a mano en el makefile. El otro, era que, no sabia como castear un jstring a char* en P1Bridge.cpp , por lo que tuve que buscar en internet y finalmente di con una solución en stack overflow https://stackoverflow.com/questions/4181934/jni-converting-jstring-to-char.

```
JNIEXPORT void JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj,jint longitudArray,jstring op,jstring multih,jint numThreads,jint numArgs) {
  //Forma de pasar un jstring a un char
  const char *nativeString;
  nativeString=env->GetStringUTFChars(op,0);
  char *ope=(char*)nativeString;
  nativeString=env->GetStringUTFChars(multih,0);
  char *multihilo=(char*)nativeString;
	todo(longitudArray,ope,multihilo,numThreads,numArgs);
	return;
}
```
Basicamente, tuve que castear las variables multihilo, que indica el tipo de ejecución que utilizaremos y op, que es el tipo de operación.
Además tuve un problema con el número de argumentos, ya que no caía en que en java no se tiene en cuenta el ejecutable del archivo como parámetro.



## 3. Metodología y desarrollo de las pruebas realizadas
  Antes de realizar los benchmarks he seguido las indicaciones que nos diste en clase, tener el portátil conectado a la corriente, sin ninguna aplicación o proceso en funcionamiento y recién encendido.Además, de fijar la frecuencia de procesador, lo cual me generó bastantes problemas y tuve que buscar bastante en internet. Al final encontré que era un problema con el kernel y el grub, ya que en la versión de mi sistema hay unicamente dos governors *powersave* y *performance* por lo que tuve que deshabilitar el driver *intel_pstate* desde la grub. Tras esto, ya pude establecer el governor como userspace e imponer una frecuencia de 2.2GHz para los 8 cores de mi portátil.
  Antes de nada realizo unas ejecuciones de calentamiento, para preparar el ordenador para la ejecuciones.

  En este primer benchmark podemos ver la comparación al ejecutar el programa secuencia y el multihilo,en ella esta representada el speed up frente al numero de elementos para un número de hilos que va del 2 al 12.<br>

  ![](images/benchmark1.png)

Podemos observar que el speedup del secuencial es 1, ya que se realiza 1 ejecución cada instante de tiempo.Además podemos observar, que para números de elementos pequeños(100000 elementos) el speed up es parecido, e incluso, realizando pruebas para menor número de elementos se observa que el secuencial, como es lógico, tarda menos, ya que los threads tienen que inicializarse, hacer sus respectivas llamadas...etc.En cambio, podemos observar que al aumentar el número de elementos, el speedup mejora con creces, por ejemplo para 500 millones de elementos, el speedup es 4 respecto al secuencial.




  En esta gráfica estamos comparando la ejecución multi-hilo frente al optimize, en ella sale representada en el eje de las y los speed up y en el de las x el número de elementos, la gráfica en si esta un poco sucia, ya que he tenido que dejar una columna libre para que se diferenciara bien las dos comparaciones para n número de hilos.

  ![](images/benchmark2.png)

  Podemos observar que los speedup no varían mucho excepto en algunos casos, optimize 4 hilos(100.000 elementos), optimize 8 hilos(10.000.000 elementos) y optimize 4 hilos(500.000.000 elementos), pero no los vamos a considerar. Esto se debe a que la mejora del optimize y más concreto, el uso de variables átomicas para nuestro problema no varía en gran medida el tiempo de ejecución


## 4. Discusión

Esta práctica me ha servido para recordar los supuestos conocimientos que teníamos de c de asignaturas anteriores como Sistemas Operativos y para aprender lo básico de c++, además de aprender la gran ventaja que supone las implementaciones paralelas frente a las secuenciales en algunos programas.Cabe destacar que la práctica en si ha sido de gran dificultad, ya que a parte de lo mencionado anterior todo era nuevo, el puente entre java y c y los benchmarks lo cuál me ha resultado con diferencia lo más difícil porque hay que tener en cuenta un montón de parámetros, que en caso contrario, puede ocasionar que las gráficas sean incorrectas y entonces se llegue a conclusiones erróneas o incoherentes.Sin embargo, quitando esto último, los benchmarks me han parecido buena forma para analizar el rendimiento de un programa, además de que, seguramente, tengamos que trabajarlos en un futuro, por lo que me parece correcto empezar a manejarlos ahora.

Para los benchmarks he utilizado la API gettimeofday ya que me ha parecido la forma mas sencilla de calcular los tiempos de ejecución.
