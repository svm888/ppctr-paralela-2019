public class P1 {
   public static void main(String[] args) {
     int lArray=Integer.parseInt(args[0]);
     String operacion=args[1];
     int numThreads=0;
     int numArgs=args.length;
     String multih;
     if(numArgs==2){
        multih="";
     }else{
        multih=args[2];
        numThreads=Integer.parseInt(args[3]);
      }
    (new P1Bridge()).compute(lArray,operacion,multih,numThreads,numArgs);
   }
}
