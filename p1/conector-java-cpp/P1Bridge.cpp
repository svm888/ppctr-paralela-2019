#include <jni.h>
#include "P1Bridge.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <thread>
#include <mutex>
#include <condition_variable>

#define DEBUG 1
#define FEATURE_LOGGER 1
#define FEATURE_OPTIMIZE 0

#if FEATURE_LOGGER==1
//Variable condicional global
std::condition_variable g_cond;
bool listo =false;
double *resTotal;
//Numero de threads que han terminado
int contador=0;
#endif

//Prototipos de funciones
void paralelo(int,int,int, double*,int,int,int,double*,int);
void logger(int ,int,double *,std::condition_variable *,bool *);

bool hayResto=false;
//Mutex para controlar la operacion
std::mutex g_op;
//Mutex para controlare el resto en caso en el que lo haya
std::mutex g_resto;
//Mutex para controlar que los resultados de los threads se pasen de uno en uno
std::mutex g_m;
int resto=0;
int eleRestantes=0;

void todo (int longitudArray,char *operacion,char *multih,int numThreads,int numArgs) {
  //En caso de recibir un numero de argumentos invalidos
if(numArgs!=2 && numArgs!=4){
  printf("Numero de argumentos invalidos \n");
  exit(-1);
}
//Variables para calcular el tiempo de ejecucion
struct timeval ti,tf;
double tiempo;

int numHilos;
//Ejecucion paralela
  numHilos=numThreads;
  if(numHilos>12){
    printf("Numero maximo de threads alcanzado \n");
    exit(-1);
  }

//Solucion de la operacion
double solucion=0;
//Array a utilizar
int numEle=longitudArray;

//Obtenemos el tipo de operacion
char *tipoOperacion=operacion;

//Obtenemos el espacio en memoria necesario
double* arrayOperaciones=(double*)malloc(sizeof(double) * numEle);

//Rellenamos el array
for(int i=0;i<numEle;i++){
  arrayOperaciones[i]=i;
}

#if DEBUG==1
printf("Tipo operación: %s\n",tipoOperacion );
printf("Número de elementos:%d\n",numEle );
#endif

int op;
if(strcmp(tipoOperacion,"sum")==0){
  op=0;
}else if(strcmp(tipoOperacion,"sub")==0){
  op=1;
}else{
  op=2;
}

if(numArgs==2){
  /*
  *								*
  PARTE SECUENCIAL
  *								*
  */


  gettimeofday(&ti,NULL);
  //Si el tipo de operación es suma
  if(op==0){
    for(int i=0;i<numEle;i++){
      solucion=solucion+arrayOperaciones[i];
    }
  }
  //Si el tipo de operación es resta
  if(op==1){
    for(int i=0;i<numEle;i++){
      solucion=solucion-arrayOperaciones[i];
    }
  }
  //Si el tipo de operación es xor
  if(op==2){
    for(int i=0;i<numEle;i++){
      solucion=(int)solucion^(int)arrayOperaciones[i];
    }
  }

  gettimeofday(&tf,NULL);
  tiempo = (double)(tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
  #if DEBUG ==1
  printf("\n");
  printf("///////////SECUENCIAL////////////\n");
  printf("Tiempo empleado secuencial: %f ms\n", tiempo);
  printf("Resultado de las operaciones: %f\n",solucion);
  #endif
}

  if(numArgs==4){
  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////
  /*
  *								*
  PARTE MULTI-THREAD
  *								*
  */
  //thread
  solucion=0;
  std::thread threads[numHilos];

  //Tamaño del subvector que se llevará cada hilo
  //El tamaño indica el numero de elementos que se llevara cada thread
  int tam=numEle/numHilos;

  if(numEle%numHilos!=0){
    eleRestantes=numEle-tam*numHilos;
    resto=arrayOperaciones[numEle-tam];
    hayResto=true;
  }

  //Si el numero de elementos es mayor que el numero de hilos solo
  //necesitamos un numero de hilos igual al numero de elementos
  if(numEle<numHilos){
    numHilos=numEle;
  }


  #if DEBUG==1
  printf("\n");
  printf("///////////MULTI HILO////////////\n");
  printf("Cada thread se encarga de %d elementos\n",tam);
  printf("Elementos restantes: %d\n",eleRestantes);
  #endif



  #if FEATURE_LOGGER==1
  std::condition_variable cv;
  //Hilo del logger
  std::thread log;
  double resLogger=0;
  double arrayLogger[numHilos];
  resTotal=arrayLogger;
  //True si ha terminado el logger
  bool finLog=false;
  log=std::thread(logger,numHilos,op,&resLogger,&cv,&finLog);
  #endif

  //Empieza el tiempo de ejecucion
  gettimeofday(&ti,NULL);
  for(int i=0; i<numHilos;i++){
    //Calculamos la posicion del array perteneciente a cada thread con el que le tocara trabajar
    int posThread=i*tam;
    threads[i]=std::thread(paralelo,op,posThread,resto,arrayOperaciones,tam,numHilos,numEle,&solucion,i);
  }

  for(int i=0; i<numHilos;i++){
    threads[i].join();
  }

  #if FEATURE_LOGGER==1
  std::unique_lock<std::mutex> lm(g_m);
  cv.wait(lm, [&finLog] {return finLog;});
  log.join();
  lm.unlock();
  #if DEBUG==1
  if(solucion==resLogger){
    printf("Logger correcto.Resultado logger: %f,Resultado threads:%f\n ",resLogger,solucion);
  }else{
    printf("Logger incorrecto.Resultado logger: %f,Resultado threads:%f\n",resLogger,solucion);
  }
  #endif
  #endif
  //La ejecucion termina, por lo que se para el tiempo
  gettimeofday(&tf,NULL);
  tiempo = (double)(tf.tv_sec-ti.tv_sec)*1000+(tf.tv_usec-ti.tv_usec)/1000.0;
  free(arrayOperaciones);

  #if DEBUG ==1
  printf("Tiempo empleado multi-thread: %f ms\n", tiempo);
  printf("Resultado de las operaciones: %f\n",solucion);
  #endif

  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////
}
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//FUNCIONES//
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
#if FEATURE_LOGGER==1
void logger(int numHilos,int op,double *resLogger,std::condition_variable *cv,bool *finLog){
  double resultado=0;
  double arrayLogger[numHilos];
  int i;
  while(contador !=numHilos){
    std::unique_lock<std::mutex>lock(g_m);
    g_cond.wait(lock,[]{return listo;});
    listo=false;
    lock.unlock();
  }
  for(i; i<numHilos;i++){
    arrayLogger[i]=resTotal[i];
  }
  for(i=0;i<numHilos;i++){
    printf("El thread %d tiene como resultado del log:%lf.\n", i, arrayLogger[i]);
  }
  for(i=0;i<numHilos;i++){
    if(op==0){
      resultado=resultado+arrayLogger[i];
    }
    if(op==1){
      resultado=resultado-arrayLogger[i];
    }
    if(op==2){
      resultado=(int)resultado^(int)arrayLogger[i];
    }
  }
  #if DEBUG==1
  printf("Resultado logger:%f\n",resultado);
  #endif
  *resLogger=resultado;
  *finLog=true;
  cv->notify_one();
}
#endif


void paralelo(int op,int posThread,int resto, double* vector,int tam,int numHilos,int numEle,double* resultado,int idThread){
  //Resultado que obtendrá cada hilo y que se sumará al resultado total
  long resParcial=0;
  //El bucle recorre desde la primera posicion del array perteneciente al hilo actual
  // mas el tamaño del subarray (numEle/numHilos), es decir hasta tam+posThread
  for(int i=posThread;i<posThread+tam;i++){
    if(op==0){
      resParcial+=(long)vector[i];
    }
    //Si el tipo de operación es resta
    if(op==1){
      resParcial-=(long)vector[i];
    }
    //Si el tipo de operación es xor
    if(op==2){
      resParcial=(int)resParcial^(int)vector[i];
    }
  }

  //Si hay resto el primer thread que termina se encarga de la parte sobrante
  if (hayResto){
    std::lock_guard<std::mutex>guard(g_resto);
    if(op==0){
      resParcial=resParcial+resto;
    }
    if(op==1){
      resParcial=resParcial-resto;
    }
    if(op==2){
      resParcial=(int)resParcial^(int)resto;
    }
    hayResto=false;
    resto=0;
  }

  std::lock_guard<std::mutex>guard(g_op);
  #if FEATURE_LOGGER ==1
  resTotal[idThread]=resParcial;
  contador++;
  if(!listo){
    listo=true;
  }
  g_cond.notify_one();
  #endif
  *resultado+=resParcial;

}




JNIEXPORT void JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj,jint longitudArray,jstring op,jstring multih,jint numThreads,jint numArgs) {
  //Forma de pasar un jstring a un char
  const char *nativeString;
  nativeString=env->GetStringUTFChars(op,0);
  char *ope=(char*)nativeString;
  nativeString=env->GetStringUTFChars(multih,0);
  char *multihilo=(char*)nativeString;
	todo(longitudArray,ope,multihilo,numThreads,numArgs);
	return;
}
